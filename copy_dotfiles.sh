#!/bin/bash

cp -r ~/.config/alacritty ./ 
cp -r ~/.config/sway ./
cp -r ~/.config/waybar ./
